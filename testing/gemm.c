#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <cblas.h>
#include <f77blas.h>
#include <math.h>
#include <pthread.h>
#include <assert.h>
#include "gpuErrorCheck.h"

//#include <blasx_internal.h>
void Fill_Double(double *mat, int size1, int size2)
{
    int i;
    for (i = 0; i< (double)size1*(double)size2; i++) {
        mat[i] = (double)rand()/(double)RAND_MAX;
    }
}

void Fill_Float(float *mat, int size1, int size2)
{
    int i;
    for (i = 0; i< (double)size1*(double)size2; i++) {
        mat[i] = (float)rand()/(float)RAND_MAX;
    }
}


void test_BLASX(const size_t M, const size_t N, const size_t K){

    double alpha_f = (double)(((double) rand()/(double)RAND_MAX)*10)+1;
    double beta_f  = (double)(((double) rand()/(double)RAND_MAX)*10)+1;
    double *A_f, *B_f, *C_f;
    
    CHECK_CUDA( cudaMallocHost( (void**)&A_f, M*K*sizeof(double)) );
    CHECK_CUDA( cudaMallocHost( (void**)&B_f, K*N*sizeof(double)) );
    CHECK_CUDA( cudaMallocHost( (void**)&C_f, M*N*sizeof(double)) );
    
    Fill_Double(A_f,M,K);
    Fill_Double(B_f,K,N);
    Fill_Double(C_f,M,N);
    
    cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans,
                M, N, K,
                alpha_f, A_f,M,
                B_f, K,
                beta_f, C_f, M);
    
    CHECK_CUDA( cudaFreeHost(A_f) );
    CHECK_CUDA( cudaFreeHost(B_f) );
    CHECK_CUDA( cudaFreeHost(C_f) );
    
}

int
main(int argc, char **argv)
{
 
    size_t m_array[13]={1000,2024,4072,8168, 16360, 32744, 65512, 131048,
        262120, 524264,1048552, 2097128, 4194280};
    size_t i = 0, max_iter = 0;
    size_t m=0, n=0, k=0;
    // warm up
    test_BLASX(10000, 10000, 10000);

    // test 1
    k = 1000; n = 1000;
    max_iter = 12;
    for(i = 0; i < max_iter; i++){
        m = m_array[i];
        test_BLASX(m, n, k);
    }
    //test 2
    k = 5000; n = 5000;
    max_iter = 9;
    for(i = 0; i < max_iter; i++){
        m = m_array[i];
        test_BLASX(m, n, k);
        
    }
    // test 3
    k= 5000; n = 500;
    max_iter = 9;
    for(i = 0; i < max_iter; i++){
        m = m_array[i];
        test_BLASX(m, n, k);
    }
    // test 4
    k = 1000; n = 10000;
    max_iter = 8;
    for(i = 0; i < max_iter; i++){
        m = m_array[i];
        test_BLASX(m, n, k);
    }

}

