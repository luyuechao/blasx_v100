#Those variables have to be changed accordingly!
# Compilers, linker/loaders, the archiver, and their options.

CC        = gcc
FC        = gfortran
CPLUS     = g++

ARCH      = ar
ARCHFLAGS = cr
RANLIB    = ranlib

CFLAGS    =

# Blas Library
LIBCPUBLAS = "/usr/lib/libopenblas.so"
#LIBCPUBLAS = "/System/Library/Frameworks/Accelerate.framework/Frameworks/vecLib.framework/Versions/Current/libBLAS.dylib"
LIBGPUBLAS = -L/usr/local/cuda/lib64 -lcudart -lcublas -lcuda
LIBBLAS   = $(LIBGPUBLAS)

#BLAS Include
INCBLAS   = -I/usr/local/cuda/include
NVCCFLAGS += -gencode arch=compute_70,code=sm_70 -gencode arch=compute_70,code=compute_70
CFLAGS    += -DMIN_CUDA_ARCH=700
CXXFLAGS  += -DMIN_CUDA_ARCH=700
